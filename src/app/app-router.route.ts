import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router'
import { RandomImageComponent } from './random-image/random-image.component'
import { SplashPageComponent } from './splash-page/splash-page.component';
import { RandomOrbComponent } from './random-orb/random-orb.component';
import { GeneratorPageComponent } from './generator-page/generator-page.component';

@NgModule({
	imports: [
		RouterModule.forRoot([
		{
			path: '',
			component: SplashPageComponent
		},
		{
			path: 'generate',
			component: GeneratorPageComponent
		}		
		])
	],
	exports: [
		RouterModule
	]
})

export class AppRouterModule {
	
}
