import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { RandomImageComponent } from './random-image/random-image.component';
import { AppRouterModule } from './app-router.route';
import { SplashPageComponent } from './splash-page/splash-page.component';
import { RandomOrbComponent } from './random-orb/random-orb.component';
import { GeneratorPageComponent } from './generator-page/generator-page.component';

@NgModule({
  declarations: [
    AppComponent,
    RandomImageComponent,
    SplashPageComponent,
    RandomOrbComponent,
    GeneratorPageComponent
  ],
  imports: [
    BrowserModule,
    AppRouterModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
