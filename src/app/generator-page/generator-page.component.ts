import { Component, ViewChild, ElementRef } from '@angular/core';
import { Http } from '@angular/http';
import {Router} from '@angular/router'
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-generator-page',
  templateUrl: './generator-page.component.html',
  styleUrls: ['./generator-page.component.css']
})
export class GeneratorPageComponent {

  @ViewChild('numDownload') numDownload : ElementRef;

  randomImageUrl;
  
  init;

  constructor(private http : Http, private router : Router) { 
  	this.randomImageUrl = "http://96.231.134.161:8080/InfiniteImagesBackend-0.0.1-SNAPSHOT/random-image/256/256#junk";
  }

  regenerateRandomImage() {
    this.randomImageUrl = this.randomImageUrl + "a";
  }

  downloadXImages() {
  	window.location.href = "http://96.231.134.161gç:8080/InfiniteImagesBackend-0.0.1-SNAPSHOT/givemeimages/" + this.numDownload.nativeElement.value + "/256/256";
  }

  str2bytes(str) {
    var bytes = new Uint8Array(str.length);
    for (var i=0; i<str.length; i++) {
        bytes[i] = str.charCodeAt(i);
    }
    return bytes;
}

 

}
