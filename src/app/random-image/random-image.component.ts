import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

@Component({
  selector: 'app-random-image',
  templateUrl: './random-image.component.html',
  styleUrls: ['./random-image.component.css']
})
export class RandomImageComponent implements OnInit {

  fromServer;
  // Appease the compiler
  init;

  constructor(private http : Http) { 
  	this.fromServer = "junk";
  	this.http.get("http://52.14.254.231:8080/InfiniteImagesBackend-0.0.1-SNAPSHOT/random-image")
  	.subscribe(
  		data => {
  			this.fromServer = data['_body'];
  		},
  		err => {
			console.log(err.json());  		
  		}
  	)
  }

  ngOnInit() {
  }

}
