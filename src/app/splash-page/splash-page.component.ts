import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'

@Component({
  selector: 'app-splash-page',
  templateUrl: './splash-page.component.html',
  styleUrls: ['./splash-page.component.css']
})
export class SplashPageComponent implements OnInit {

  fileLocation : string;

  // Appease the compiler
  init;
  
  constructor(private router : Router) { 
  	this.fileLocation = "littletwofiftysix.png";
  }

  ngOnInit() {}

  goToGeneratePage() {
  	this.router.navigateByUrl('/generate');
  }

}
