import { Component, OnInit, HostListener } from '@angular/core';
import { Http } from '@angular/http';

@Component({
  selector: 'random-orb',
  templateUrl: './random-orb.component.html',
  styleUrls: ['./random-orb.component.css']
})
export class RandomOrbComponent implements OnInit {

  isMouseDown : boolean;

  width : number;
  height : number;

  posXNum : number;
  posX : string;
  posYNum : number
  posY : string;

  prevX : number;
  prevY : number;
  
  // Appease the compiler
  onFocusLost;
  init;

  constructor() {
    let screenWidth = this.getWidth();
    let screenHeight = this.getHeight();
    this.width = 192;
    this.height = 192;
    this.isMouseDown = false;
    this.posXNum = Math.random() * (screenWidth - this.width);
    this.prevX = this.posXNum;
    this.posYNum = Math.random() * (screenHeight - this.height);
    this.prevY = this.posYNum;
    this.posX = this.posXNum + "px";
    this.posY = this.posYNum + "px";
  }

  ngOnInit() {
  }

  private getWidth() {
    if (self.innerWidth) {
      return self.innerWidth;
    }

    if (document.documentElement && document.documentElement.clientWidth) {
      return document.documentElement.clientWidth;
    }

    if (document.body) {
      return document.body.clientWidth;
    }
  }

  private getHeight() {
    if (self.innerHeight) {
      return self.innerHeight;
    }

    if (document.documentElement && document.documentElement.clientHeight) {
      return document.documentElement.clientHeight;
    }

    if (document.body) {
      return document.body.clientHeight;
    }
   }

  @HostListener('mousedown', ['$event'])
  onMouseDown(event) {
    this.isMouseDown = true;
    this.prevX = event.clientX;
    this.prevY = event.clientY;
  }

  @HostListener('mouseleave', ['$event'])
  onMouseLeave(event) {
    this.isMouseDown = false;
  }

  @HostListener('mouseup', ['$event'])
  onMouseUp(event) {
    this.isMouseDown = false;
  }

  @HostListener('mousemove', ['$event'])
  onMousemove(event) {
    if (this.isMouseDown) {
      this.posXNum += (event.clientX - this.prevX);
      this.posX = this.posXNum + "px";
      this.posYNum += (event.clientY - this.prevY);
      this.posY = this.posYNum + "px";
      this.prevX = event.clientX;
      this.prevY = event.clientY;
    }
  }
  
  @HostListener('onFocusLost', ['$event'])
  fuck(event) {
    
  }

}
