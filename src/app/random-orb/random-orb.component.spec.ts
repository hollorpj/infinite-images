import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RandomOrbComponent } from './random-orb.component';

describe('RandomOrbComponent', () => {
  let component: RandomOrbComponent;
  let fixture: ComponentFixture<RandomOrbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RandomOrbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RandomOrbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
